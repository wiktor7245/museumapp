package com.example.museumapp;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, SensorEventListener,
        LocationListener {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5445;

    private GoogleMap googleMap;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Marker currentLocationMarker;
    private Circle mCircle;
    private Location currentLocation;
    private boolean firstTimeFlag = true;
    private SensorManager mSensorManager;
    TextView locationText;
    TextView compassText;
    private float[] mGravity = new float[3];
    private float[] mGeomagnetic = new float[3];
    private int azimuth = 0;
    LocationManager lm;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            /* do what you need to do */
            currentLocationMarker.setRotation(azimuth);
            /* and here comes the "trick" */
            handler.postDelayed(this, 1000);
        }
    };

    private final View.OnClickListener clickListener = view -> {
        if (view.getId() == R.id.currentLocationImageButton && googleMap != null && currentLocation != null)
            animateCamera(currentLocation);
    };

    private final LocationCallback mLocationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            Log.i("tear","result");
            super.onLocationResult(locationResult);
            if (locationResult.getLastLocation() == null) {
                return;
            }
            currentLocation = locationResult.getLastLocation();
            if (firstTimeFlag && googleMap != null) {
                animateCamera(currentLocation);
                firstTimeFlag = false;
            }
            showMarker(currentLocation);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
        findViewById(R.id.currentLocationImageButton).setOnClickListener(clickListener);
        Log.i("tear","create");
        locationText = (TextView) findViewById(R.id.information);
        compassText = (TextView) findViewById(R.id.compass);
        compassText.setText("ELo");
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        //checking if we have gps and network
                lm = (LocationManager)
                getSystemService(Context. LOCATION_SERVICE );
        gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER );
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        network_enabled = networkInfo != null && networkInfo.isConnected();
        //Log.i("tear","Network13" + network_enabled);
        if(!gps_enabled){
            showSettingAlert("gps_enabled");
        }
        if(!network_enabled){
            showSettingAlert("network_enabled");
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    private void startCurrentLocationUpdates() {
        Log.i("tear","updates");
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(3000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MapsActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                return;
            }
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status)
            return true;
        else {
            if (googleApiAvailability.isUserResolvableError(status))
                Toast.makeText(this, "Please Install google play services to use this application", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Toast.makeText(this, "Permission denied by uses", Toast.LENGTH_SHORT).show();
            else if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                startCurrentLocationUpdates();
        }
    }

    private void animateCamera(@NonNull Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLng)));
    }

    @NonNull
    private CameraPosition getCameraPositionWithBearing(LatLng latLng) {
        return new CameraPosition.Builder().target(latLng).zoom(20).build();
    }

    private void showMarker(@NonNull Location currentLocation) {
        LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        Log.i("tear", "ELO");
        if (currentLocationMarker == null){
            locationText.setText("GPS accurancy: " + currentLocation.getAccuracy() + "m");
            drawMarkerWithCircle(latLng);
        }
        else{
            updateMarkerWithCircle(latLng);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (fusedLocationProviderClient != null)
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        mSensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isGooglePlayServicesAvailable()) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            startCurrentLocationUpdates();
        }
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fusedLocationProviderClient = null;
        googleMap = null;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        final float alpha = 0.97f;
        synchronized (this){
            if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
                mGravity[0] = alpha*mGravity[0] + (1-alpha)*event.values[0];
                mGravity[1] = alpha*mGravity[1] + (1-alpha)*event.values[1];
                mGravity[2] = alpha*mGravity[2] + (1-alpha)*event.values[2];
            }
            if(event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){
                mGeomagnetic[0] = alpha*mGeomagnetic[0] + (1-alpha)*event.values[0];
                mGeomagnetic[1] = alpha*mGeomagnetic[1] + (1-alpha)*event.values[1];
                mGeomagnetic[2] = alpha*mGeomagnetic[2] + (1-alpha)*event.values[2];
            }

            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R,I,mGravity,mGeomagnetic);
            if(success){
                float orientation[] = new float[3];
                SensorManager.getOrientation(R,orientation);
                azimuth = (int)Math.toDegrees(orientation[0]);
                azimuth = (azimuth +360)%360;
                if((azimuth >= 330) || ((azimuth >= 0) && (azimuth < 30))){
                    compassText.setText("Compass read: N (" + String.valueOf(azimuth)+ "°)");
                }else if((azimuth >= 30) && (azimuth < 60)){
                    compassText.setText("Compass read: NE (" + String.valueOf(azimuth)+ "°)");
                }else if((azimuth >= 60) && (azimuth < 120)){
                    compassText.setText("Compass read: E (" + String.valueOf(azimuth)+ "°)");
                }else if((azimuth >= 120) && (azimuth < 150)){
                    compassText.setText("Compass read: SE (" + String.valueOf(azimuth)+ "°)");
                }else if((azimuth >= 150) && (azimuth < 210)){
                    compassText.setText("Compass read: S (" + String.valueOf(azimuth)+ "°)");
                }else if((azimuth >= 210) && (azimuth < 240)){
                    compassText.setText("Compass read: SW (" + String.valueOf(azimuth)+ "°)");
                }else if((azimuth >= 240) && (azimuth < 300)){
                    compassText.setText("Compass read: W (" + String.valueOf(azimuth)+ "°)");
                }else{
                    compassText.setText("Compass read: NW (" + String.valueOf(azimuth)+ "°)");
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void updateMarkerWithCircle(LatLng position) {
        mCircle.setCenter(position);
        mCircle.setRadius(currentLocation.getAccuracy());
        //idk if change this
        MarkerAnimation.animateMarkerToGB(currentLocationMarker, position, new LatLngInterpolator.Spherical());
        locationText.setText("GPS accurancy: " + currentLocation.getAccuracy());
        Log.i("tear", String.valueOf(currentLocation.getAccuracy()));
        handler.postDelayed(runnable, 1000);
    }

    private void drawMarkerWithCircle(LatLng position){
        double radiusInMeters = currentLocation.getAccuracy();

        CircleOptions circleOptions = new CircleOptions().center(position).radius(radiusInMeters).fillColor(Color.parseColor("#500084d3")).strokeColor(Color.BLUE).strokeWidth(8);
        mCircle = googleMap.addCircle(circleOptions);

        currentLocationMarker = googleMap.addMarker(new MarkerOptions().icon(bitmapDescriptorFromVector(MapsActivity.this, R.drawable.ic_navigation_black_24dp)).position(position).flat(true).rotation(azimuth));
    }

    public void showSettingAlert(final String type)
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapsActivity.this);
        if("gps_enabled".equals(type)){
            alertDialog.setTitle("GPS settings!");
            alertDialog.setMessage("GPS is not enabled, Do you want to go to settings menu? ");
        }
        if("network_enabled".equals(type)){
            alertDialog.setTitle("Internet settings!");
            alertDialog.setMessage("You can speed up localization process by enabling internet. Do you want to go to settings menu? ");
        }
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if("gps_enabled".equals(type)){
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    MapsActivity.this.startActivity(intent);
                }
                if("network_enabled".equals(type)){
                    Intent intent = new Intent(Settings.ACTION_DATA_USAGE_SETTINGS);
                    MapsActivity.this.startActivity(intent);
                }
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location lastLocation) {
    }
}